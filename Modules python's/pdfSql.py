#!/bin/bash
. $HOME/.bash_profile
. ~/.db
. ~/.env
sendData=$(date "+%d%m%Y%H%M%S")
seckinn_csv=/path/script/seckin_"$sendDate".csv
DB_USER=seckins
DB_PASS=*****
DB_URL=seckin-scan:1010/Seckins
#esckin
sqlplus -s $DB_USER/$DB_PASS@$DB_URL << EOF
SPOOL $seckinn_csv
SET HEADING ON
SET echo OFF
SET feedback OFF
SET trimspool ON
SET newpage NONE
SET verify OFF
SET define OFF
SET termout OFF
SET timing OFF
SET linesize 700
SET pagesize 0
PROMPT outcome, count
select /*+ PARALLEL(8) */outcome,count(outcome)
FROM seckin.view
where OUTCOMETIME >sysdate-1/24
AND OUTCOMETIME <sysdate
group by pyoutcome;
SPOOL OFF
EXIT
EOF

****************

#!/usr/local/bin/python3
import os
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
PATH= '/path/script/test_/'
def drawGraph(pathname, titleName):
 
 fileNames=os.listdir(pathname)
 fileNames=[file for file in fileNames if '.csv' in file]
 
 for file in fileNames:
 
 df=pd.read_csv(pathname + file, index_col=0)
 df['outcome']=df['outcome']*100
 fileName=os.path.splitext(file)[0]
 
 plt.plot(df, label=fileName, marker='o', linewidth=2.0)
 plt.xlabel('Date')
 plt.ylabel(titleName)
 plt.grid(True)
 plt.xticks(rotation=60)
 plt.tight_layout()
 ax = plt.subplot(111)
 chartBox = ax.get_position()
 ax.set_position([chartBox.x0, chartBox.y0, chartBox.width*0.6, chartBox.height])
 ax.legend(loc='upper center', bbox_to_anchor=(1.30, 0.8), shadow=True, ncol=1)
fig1=plt.figure()
drawGraph(pathname=PATH, titleName="app_Outcome") 
with PdfPages(r'/u01/jenkins_script/test_fulfill/seckin_yanimda.pdf') as pdf:
 pdf.savefig(fig1)

pdf send.sh ;
#!/bin/bash
pdfFile=/path/script/test_/seckins.pdf
echo "Hi,
outcome seckin
Thank you,
Kind Regards." | mail -s "PDF_Outcome" -r Seckin@mail -a "$pdfFile" seckin@mail

*****************************Rate ****************************

#!/bin/bash
. $HOME/.bash_profile
. ~/.db
. ~/.env
spend_csv=/path/outcome1.csv
DB_USER=seckin
DB_PASS=****
DB_URL=seckinscan:1010/seckin
#SPEND
sqlplus -s $DB_USER/$DB_PASS@$DB_URL << EOF
SPOOL $spend_csv
SET HEADING ON
SET echo OFF
SET feedback OFF
SET trimspool ON
SET newpage NONE
SET verify OFF
SET define OFF
SET termout OFF
SET timing OFF
SET linesize 700
SET pagesize 0
PROMPT Date,rate
SELECT DF.REPORTDATE ||','|| DF.RATE 
FROM seckin.rate df 
where DF.REPORTDATE> sysdate -7 and
DF.FOLDERNAME='seckin' and DF.FILENAME='outcomeclear'
ORDER BY DF.REPORTDATE asc;
SPOOL OFF
EXIT
EOF

********************

#!/usr/local/bin/python3
import os
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
PATH= '/path/outcome1/'''

def drawGraph(pathname, titleName):
 
 fileNames=os.listdir(pathname)
 fileNames=[file for file in fileNames if '.csv' in file]
 
 for file in fileNames:
 
 df=pd.read_csv(pathname + file, index_col=0)
 df['Rate']=df['Rate']*100
 fileName=os.path.splitext(file)[0]
 
 plt.plot(df, label=fileName, marker='o', linewidth=2.0)
 plt.xlabel('Date')
 plt.ylabel(titleName)
 plt.grid(True)
 plt.xticks(rotation=60)
 plt.tight_layout()
 ax = plt.subplot(111)
 chartBox = ax.get_position()
 ax.set_position([chartBox.x0, chartBox.y0, chartBox.width*0.6, chartBox.height])
 ax.legend(loc='upper center', bbox_to_anchor=(1.30, 0.8), shadow=True, ncol=1)
fig1=plt.figure()
drawGraph(pathname=PATH, titleName="Outcome1 Rate (%)")
fig2=plt.figure() 
drawGraph(pathname=PATH2, titleName="outcome2 rate(%)")
with PdfPages(r'/path/Rate1.pdf') as pdf:
 pdf.savefig(fig1)
 pdf.savefig(fig2)
with PdfPages(r'/path/outcome Rate2.pdf') as pdf:
 pdf.savefig(fig4)
 pdf.savefig(fig6)
 

########################


pip3 install matplotlib --proxy="https://esckins@proxy.esckin.com.tr:80" --trusted-host files.pythonhosted.org --trusted-host pypi.org --trusted-host pypi.python.org





*****mail.sh
#!/bin/bash
pdfFile=/path/ outcome1 Rate.pdf
pdfFile2=/path/outcome2 Rate.pdf
echo "Hello,
MCCM Fulfilment rate graphs are attached as weekly trend.
Thank you,
Kind Regards." | mail -s "RATE GRAPHS" -r seckin@alert -a "$pdfFile" -a "$pdfFile2" seckin@mail.com
#rm /path/order.csv
