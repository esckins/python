# -*- coding: utf-8 -*-
"""
Created on Tue Oct  25 22:37:05 2021

@author: Seçkin
"""

# regex ileri düzey regex ve cookbook gerektirir.

#cookbooks "şöyle yapılır" tarzı handbooklardır

#Regex -- relgular expression
 """
Meta Karakterler Anlamı
. (nokta) ‘\n’ dışındaki herhangi bir karakter
? Solundaki karakterden 0 tane ya da 1 tane 
* Solundaki karakterden 0 tane ya da çok tane
+ Solundaki karakterden 1 tane ya da çok tane
{n} Burada n bir sayıdır. Solundaki karakterden tam olarak n tane anlamına gelir.
{n,} Burada n bir sayıdır. Solundaki karakterden tam olarak en az n tane anlamına gelir.
{n,m} Burada n ve m birer sayıdır. Solundaki karakterden en az n tane en fazla m tane anlamına gelir.
[ karakterler] Köşeli parantez içerisindeki karakterlerden herhangi birisi anlamına gelir. 
[x-y] x ve y aralığındaki herhangi bir karakter
[^ ] Köşeli parantez içerisindeki karakterlerden olmayan herhangi bir karakter
\w Herhangi bir alfanümerik karakter
\W Herhangi bir alfanümerik olmayan karakter
\s Herhangi bir boşluk karakteri
\S Herhangi bir boşluk olmayan karakter
\d Herhangi bir sayısal karakter
\D Herhangi bir sayısal olmayan karakter
$ Satırın sonunun belli karakterlerle sonlanması durumu (Örneğin “kaan$”)
^ Satırın başı belli karakterlerle sonlanması durumu (Örneğin ^kaan”)
(...) Gruplama amacıyla kullanılır. Böylece bunun sağındaki metakarakterler bu grup için anlam kazanır.
| Veya anlamına gelmektedir. Örneğin "ali|veli" yazı içerisindeki "ali" veya "veli" ile uyşur.
**

[_a-zA-z]+ '_' karakterinden ve alfabetik karakterlerden oluşan karakter dizileriyle uyuşur. 
Köşeli parantez içerisindeki [a-z] gibi bir kalıbın 'a' ile 'z' arasındaki herhangi bir 
karakter anlamına geldiğini anımsayınız. 
[+-]?[0-9]+\.?[0-9]* Gerçek sayı kalıplarıyla uyuşur. Örneğin “123”, “123.45”, “-1” gibi. (Ancak “.12” ya 
da “.12” gibi kalıplarla uyuşmaz) 
([0-9]{1,3}\.){3}[0-9]{1,3} Bölümleri “.” ile ayrılmış IP numaralarıyla uyuşur. Örneğin “192.160.0.100” gibi. 
Burada parantezler gruplama amacıyla kullanılmıştır. Dolayısıyla kalıbın [0-9]{1,3} 
kısmı 0’dan 9’a kadar karakterlerden 1 ya da 2 ya da 3 tane olacağını belirtir. 
kalıbın ([0-9]{1,3}\.){3} kısmı ise üç basamağa kadar sayı ve noktaların toplamda 
üç tane bulunacağını belirtmektedir. 
^\w* Satırların başındaki sözcüklerle uyuşur
"""



#pythonda binary işlemlerde yapılabilir
#a=123 ;  bin(a) fonksiyonu kullanıabilir
# print(f'{a:b}')





import re

text = 'ali veli 223 deli 128 ayşe fatma 876'
pattern = r'\d+'

result = re.findall(pattern, text)
print(result)




import re

text = 'ali veli 123 sidem 628 aysel rana 9876'
pattern = r'\d+'

result = re.findall(pattern, text)
print(result)
numbers = list(map(int, result))
print(numbers)


import re

text = 'ali ferit 14/11/2001 kemal 05/07/1955 aysel rana 1/6/2002'
pattern = r'\d\d/\d\d/\d\d\d\d'

result = re.findall(pattern, text)
print(result)



import re

text = 'ali, veli,,    selim   , aysel ,    rana'
pattern = '\w+'

result = re.findall(pattern, text)
print(result)


import re

text = 'ali21761veli123123selami987987ayşe9989898fatma'
pattern = '\d+'

result = re.split(pattern, text)
print(result)
print(result)


import re

text = 'ali veli selami 23/05/2009 ayşe fatma'
pattern = '\d\d/\d\d/\d\d\d\d'

result = re.search(pattern, text)

if result:
    print(text[result.start():result.end()])
else:
    print('cannot found..')


print()


import re

text = 'ali veli selami 123@789 ayşe fatma'
pattern = r'(\d+)@(\d+)'

result = re.search(pattern, text)

if result:
    print(result.group(0))
    print(result.group(1))
    print(result.group(2))    
else:
    print('cannot found..')







import re

text = 'ali veli selami 123@789 ayşe fatma 7890@112234 süreyya'
pattern = r'(\d+)@(\d+)'

for m in re.finditer(pattern, text):
    print(text[m.start():m.end()], m[1], m[2])






import re

text = 'ali veli selami 123@789 ayşe fatma 7890@112234 süreyya'
pattern = r'(\d+)@(\d+)'

result = re.match(pattern, text)
if result:
    print(result[0])
else:
    print('cannot find!..')






import re

text = '7890@112234'
pattern = r'(\d+)@(\d+)'

result = re.fullmatch(pattern, text)
if result:
    print(result[0])
else:
    print('cannot find!..')

import re

text = 'ali veli 12/11/1990 selami 03/05/2009 ayşe 07/11/1997 fatma'
pattern = r'\d\d/\d\d/\d\d\d\d'

result = re.sub(pattern, '----------', text)
print(result)


import re

text = 'ali veli ali, ALİ, veli ali Ali selami'
pattern = r'\wli'

result = re.findall(pattern, text, re.IGNORECASE)
print(result)
