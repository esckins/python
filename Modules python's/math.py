# -*- coding: utf-8 -*-
"""
Created on Sun Oct 31 01:43:13 2021

@author: Seçkin
"""
# Yöntem 1
import math
import math as islem

value = dir(math)
value = help(math)
value = help(math.factorial)
value = math.sqrt(49)
value = math.factorial(5)
value = math.floor(5.9)
value = math.ceil(5.9)
value = islem.factorial(5)

print(value)

# Yöntem 2

from math import *

def sqrt(x):
    print('x :'+ str(x))

from math import factorial,sqrt,ceil

value = factorial(5)
#value = sqrt(9)
#value = ceil(9.8)

print(value)