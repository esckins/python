
import json
import pandas as pd
from tabulate import tabulate

with open(r'C:\Users\admin\Desktop\data.json') as file:
    val = json.load(file)

df = pd.DataFrame()

for users in val['aggregations']['2']['buckets']:

    # User kolonu 
    user = users['key']

    # Tür kolonu
    for i in users['3']['buckets']:
        print(i)

        column_type = i['key']  # Type kolonu
        doc_count = i['doc_count']  # Count kolonu

        df = df.append({'User': user, 'Type': column_type, 'Count': doc_count}, ignore_index=True)

        # print(user, '--->', column_type, '--->', doc_count)

print(tabulate(df, headers='keys', tablefmt='psql'))

with open(r'C:\Users\admin\Desktop\output.txt', 'a') as file:
    file.truncate(0)
    file.write(tabulate(df, headers='keys', tablefmt='psql'))
