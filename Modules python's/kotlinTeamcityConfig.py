#!/usr/bin/env python

import requests
from requests.auth import HTTPBasicAuth
from urllib.parse import urljoin
import os
import io
import zipfile

# this file should contain teamcity base url
# example: https://teamcity.example.com
teamcity_url = os.environ.get('TEAMCITY_URL')
if not teamcity_url:
    with open('.teamcity-url') as teamcity_url_file:
        teamcity_url = teamcity_url_file.read()


auth_basic = os.environ.get('TEAMCITY_AUTH_BASIC')
auth_token = os.environ.get('TEAMCITY_AUTH_TOKEN')

auth = None
headers = {}

if auth_token:
    print('Auth using token')
    headers = {
        'Authorization': 'Bearer ' + auth_token,
    }
else:
    if auth_basic:
        print('Auth using basic auth from env')
    else:
        print('Auth using basic auth from file .auth')
        # this file should contain username and password for temacity admin user
        # example: username:password
        with open('.auth') as auth_file:
            auth_basic = auth_file.read()
    auth = HTTPBasicAuth(*(auth_basic.split(':')))

if not os.path.exists('kotlin'):
    os.mkdir('kotlin')

url = urljoin(teamcity_url, 'admin/versionedSettingsActions.html?projectId=_Root&action=generate&format=kotlin&version=v2019_1&useRelativeIds=true')
print(url)
r = requests.get(url, auth=auth, headers=headers)
with zipfile.ZipFile(io.BytesIO(r.content)) as zf:
    zf.extractall(path='kotlin')
