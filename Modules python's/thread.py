
#Thead Kavrami
#Çalışmakta olan programlara process , process akışlarına da thread denir

"""
Programda tek bir akis vardir.

 Entry_point -> End_point

ex:
"""

print("program started")
def foo():
    print('foo')
foo()
print('program ends ...')

####

def foox():
    for i in range(1000):
        pass:

def bar():
    for i in range(1000):
        pass

foox()
bar()

#burada akış sırasıyla çalışıyor. Bir programdan birden fazla akışta çalışıyorsa
# bu duruma thread deniyor.


"""
thread: iplik 

CPU aynı anda birden fazla akış çalıştıramaz. Makinaya güc katmak için makinalara birden cok cpu takmaya calıstılar
Böylece birim zamanda daha çok iş yapılsın anlamına geldi.

işlemcilerde core(çekirdek) = cpu kavramı hayatımıza girdi.
i7-i7863

işlemciler bölümünde (aygıt yöneticisi) daha fazla görünebilir. bu teknigin adı hyper coredur

önemli olan core adedidir.

İşletim sisteminin curnel kısmı çalışmakta olan programı sürekli izliyor aslında

Thread ise sadece bir akış. Thread processin akışlarına denir.

windows,mac, linux gibi işletim sistemleri multiprocessing ve multithreading özelliği destekler.


Zaman paylaşımlı ( time sharing )

threadleri cpuya atıyor bir süre çalıştıktan sonra digerine veriyor.
Bakıldıgı zaman hepsi aynı anda çalışıyormus gibi görünüyor.


quantum 20 ms(windows) linux/unix (60ms)'dir.

Sistemin yükünde aşırı atışlar overload duruma getirebilir.

scheduler ile eldeki kaynakları en verimli şekilde kullanıyor.


Linuxda taskstruck adında tablo tutuluyor.Taskların durumu memoryde yer alıyor.

Task switch / context switch ile de tasklar arası geçiş belirlenir ve bir süresi vardır

Dolayısıyla quanta süresi ( process calısma miktarı + switch ) ideal secilmelidir.
dogru seçilmezse trueput düşer

Cpu'ya threadler atanır.

Dolayısılya programları birden çok threade göre yazmak lazim.


bütün threadler aynı quanta süresinde mi calısır bu süreleri degistirebilir miyiz?
Thread'e torpil geçilebilir mi?

Threadlerin öncelik dereceleri ayarlanabilir.


#avantajları

- threadler arka planda periyodik işlemleri yapmak için iyi bir araçtır.
- threadler programları hızlandırmak için kullanilabilir.
- threadler gui programlarında uzun süren işlemler için mecburen kullanılır

örn. bir sorgu calıstırıldı ve thread cpu'ya gönderildi fakat uzun sürecek bir işlem
bu sürede cancel inpuls gönderilip sorgu iptal edilirse diger thread iptal edilir

yani multi thread sayesinde diger istek ile kuyruk kısaltılabildi aksi halde sorgunun bitmesini bekleyecektik.

cpu cacheleri mevcut ve cpu her threade eşit dagıtır.

aynı anda çalışma olanagı saglamak için paralel programala yapmak gerekir
threadlerin eş zamanlı da dagıtılması demektir. multithreading yapı ile paralel programlamadır esasen

thread senkronizasyon ile birbirlerini beklemesi ve aralarındaki haberlesmeyi saglaması gerekir.

concurency => iç içe yapma
multitherading
paralel programming -> aynı makine üzerinde

distributed programing -> network hattında farklı makinelera dagıtıp (genelde tcpip)


"""
