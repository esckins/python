# -*- coding: utf-8 -*-
"""
Created on Sun Oct 31 01:50:58 2021

@author: Seçkin

"""

import random

# result = dir(random)
# result = help(random)

result = random.random() # 0.0 - 1.0
result = random.random() * 100
result = int(random.uniform(10,100))
result = random.randint(1,100)

greeting = 'hi all'
names = ['ahmeti','mehmet','deniz','cemre']
# result = names[random.randint(0,len(names)-1)]

result = random.choice(names)
result = random.choice(greeting)

liste = list(range(10))
random.shuffle(liste)
result = liste

liste = range(100)
result = random.sample(liste, 3)
result = random.sample(names, 2)

print(result)




'''
   modül hakkında bilgilendirme 
'''

print('modül eklendi')

number = 10

numbers = [1,2,3]

person = {
    "name": "Ali",
    "age":"25",
    "city":"istanbul"
}

def func(x):
    '''
        fonksiyon hakkında bilgilendirme
    '''
    print(f'x: {x}')

class Person:
    def speak(self):
        print('I am speaking...')


