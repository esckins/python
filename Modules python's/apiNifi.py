envProd = "https://dynatraceone..esckin/e/111abc-222-def"
metricAPIURL="/api/v2/metrics/query?metricSelector="
metrics = ['builtin:service.requestCount.total','builtin:service.response.time','builtin:service.errors.total.rate']
operationURL= ':filter(and(in("dt.entity.service",entitySelector("type(service),entityName(~"#ENTITY#~")"))))'
splitURL = ':splitBy("dt.entity.service"):avg:auto:sort(value(avg,descending)):limit(10)'
timeFrameURL = '&from=now-3m&to=now-3m'
channels = ['ABC','DEF','KLM']

for channel in channels:
  for metric in metrics:
    url = envProd + metricAPIURL + metric + operationURL.replace("#ENTITY#",channel) + splitURL + timeFrameURL
    flowFile = session.create()
    flowFile = session.putAttribute(flowFile, 'dynaurl', url)
    flowFile = session.putAttribute(flowFile, 'metric', metric)
    flowFile = session.putAttribute(flowFile, 'channel', channel)
    session.transfer(flowFile, REL_SUCCESS)

envProd = "https://dynatraceone.esckin/e/111abc-222-def"
metricAPIURL="/api/v2/metrics/query?metricSelector="
metrics = ['builtin:service.requestCount.total','builtin:service.response.time','builtin:service.errors.total.rate']
operationURL= ':filter(and(in("dt.entity.service",entitySelector("type(service),entityName(~"#ENTITY#~")"))))'
splitURL = ':splitBy("dt.entity.service"):avg:auto:sort(value(avg,descending)):limit(10)'
timeFrameURL = '&from=now-3m&to=now-3m'
channels = ['ABC','DEF','KLM']

for channel in channels:
  for metric in metrics:
    url = envProd + metricAPIURL + metric + operationURL.replace("#ENTITY#",channel) + splitURL + timeFrameURL
    flowFile = session.create()
    flowFile = session.putAttribute(flowFile, 'dynaurl', url)
    flowFile = session.putAttribute(flowFile, 'metric', metric)
    flowFile = session.putAttribute(flowFile, 'channel', channel)
    session.transfer(flowFile, REL_SUCCESS)


urld= [
'https://dynatraceone.esckins/e/111abc-aaa1/api/v2/metrics/query?metricSelector=builtin:host.cpu.usage:filter(and(in("dt.entity.host",entitySelector("type(host),mzName(~"DynatraceElastic~")")))):names&from=now-3m&to=now-2m',..
]

for url in urld:
  if "builtin:host.cpu.usage" in url:
     metric="builtin:host.cpu.usage"
  elif "builtin:host.disk.usedPct" in url:
     metric="builtin:host.disk.usedPct"
  elif "builtin:host.mem.usage" in url:
     metric="builtin:host.mem.usage"
  elif "builtin:host.net.nic.trafficIn" in url:
     metric="builtin:host.net.nic.trafficIn"
  elif "builtin:host.net.nic.trafficOut" in url:
     metric="builtin:host.net.nic.trafficOut"
  elif "builtin:tech.generic.cpu.usage" in url:
     metric="builtin:tech.generic.cpu.usage"
  elif "builtin:tech.generic.mem.usage" in url:
     metric="builtin:tech.generic.mem.usage"
  elif "builtin:service.requestCount.total" in url:
     metric="builtin:service.requestCount.total"
  elif "builtin:service.response.time" in url:
     metric="builtin:service.response.time"
  channel="SubeClientPC"
  flowFile = session.create()
  flowFile = session.putAttribute(flowFile, 'dynaurl', url)
  flowFile = session.putAttribute(flowFile, 'metric', metric)
  flowFile = session.putAttribute(flowFile, 'channel', channel)
  session.transfer(flowFile, REL_SUCCESS)

urlElastic= ['https://dynatraceone.esckins/e/111abc-aaa1/api/v2/metrics/query?metricSelector=builtin:host.cpu.usage:filter(and(in("dt.entity.host",entitySelector("type(host),mzName(~"DynatraceElastic~")")))):names&from=now-3m&to=now-2m',..]

for url in urlMobil:
  if "builtin:host.cpu.usage" in url:
     metric="builtin:host.cpu.usage"
  elif "builtin:host.disk.usedPct" in url:
     metric="builtin:host.disk.usedPct"
  elif "builtin:host.mem.usage" in url:
     metric="builtin:host.mem.usage"
  elif "builtin:host.net.nic.trafficIn" in url:
     metric="builtin:host.net.nic.trafficIn"
  elif "builtin:host.net.nic.trafficOut" in url:
     metric="builtin:host.net.nic.trafficOut"
  elif "builtin:tech.generic.cpu.usage" in url:
     metric="builtin:tech.generic.cpu.usage"
  elif "builtin:tech.generic.mem.usage" in url:
     metric="builtin:tech.generic.mem.usage"
  elif "builtin:tech.generic.network.traffic.traffic" in url:
     metric="builtin:tech.generic.network.traffic.traffic"
  elif "builtin:tech.generic.processCount" in url:
     metric="builtin:tech.generic.processCount"
  elif "builtin:tech.dotnet.gc.timePercentage" in url:
     metric="builtin:tech.dotnet.gc.timePercentage"
  elif "builtin:service.requestCount.total" in url:
     metric="builtin:service.requestCount.total"
  elif "builtin:service.response.time" in url:
     metric="builtin:service.response.time"
  elif "builtin:service.nonDbChildCallTime" in url:
     metric="builtin:service.nonDbChildCallTime"
  elif "builtin:service.nonDbChildCallCount" in url:
     metric="builtin:service.nonDbChildCallCount"
  elif "builtin:service.dbChildCallTime" in url:
     metric="builtin:service.dbChildCallTime"
  elif "builtin:service.dbChildCallCount" in url:
     metric="builtin:service.dbChildCallCount"
  elif "builtin:service.dbconnections.failure" in url:
     metric="builtin:service.dbconnections.failure"
  elif "builtin:service.errors.server.rate" in url:
     metric="builtin:service.errors.server.rate"
  elif "builtin:service.successes.server.rate" in url:
     metric="builtin:service.successes.server.rate"
  elif "builtin:service.keyRequest.count.total" in url:
     metric="builtin:service.keyRequest.count.total"
  elif "builtin:service.keyRequest.response.time" in url:
     metric="builtin:service.keyRequest.response.time"
  elif "builtin:service.keyRequest.nonDbChildCallTime" in url:
     metric="builtin:service.keyRequest.nonDbChildCallTime"
  elif "builtin:service.keyRequest.dbChildCallTime" in url:
     metric="builtin:service.keyRequest.dbChildCallTime"
  elif "builtin:service.keyRequest.dbChildCallCount" in url:
     metric="builtin:service.keyRequest.dbChildCallCount"
  elif "builtin:service.keyRequest.nonDbChildCallCount" in url:
     metric="builtin:service.keyRequest.nonDbChildCallCount"
  elif "builtin:service.keyRequest.successes.server.rate" in url:
     metric="builtin:service.keyRequest.successes.server.rate"
  elif "builtin:service.keyRequest.errors.server.rate" in url:
     metric="builtin:service.keyRequest.errors.server.rate"
  channel="elastic"
  flowFile = session.create()
  flowFile = session.putAttribute(flowFile, 'dynaurl', url)
  flowFile = session.putAttribute(flowFile, 'metric', metric)
  flowFile = session.putAttribute(flowFile, 'channel', channel)
  session.transfer(flowFile, REL_SUCCESS)
