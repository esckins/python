import json
import pandas as pd
from tabulate import tabulate


with open(r'/py_elastic/seckins/audit/ReportAudit/data.json') as file:
    val = json.load(file)

df = pd.DataFrame()

for users in val['aggregations']['2']['buckets']:

    # User kolonu
    user = users['key']

    # Type kolonu
    for i in users['3']['buckets']:
        #print(i)

        column_type = i['key']  # Type kolonu
        doc_count = i['doc_count']  # Count kolonu

        df = df.append({'User': user, 'Type': column_type,'Count': doc_count}, ignore_index=True, sort=False)
        df2=df.loc[:,['User','Type','Count']]
        # print(user, '------>', column_type, '------>', doc_count)
        #df2_html = df2.to_html()
#print(tabulate(df, headers='keys', tablefmt='psql'))

with open(r'/py_elastic/seckins/audit/ReportAudit/output.txt', 'a') as file:
    file.truncate(0)
    file.write(tabulate(df2, headers='keys', tablefmt='psql'))
#file.write(tabulate(df2, headers='keys', tablefmt='html'

#powered by Harun Özgür Tişkaya
