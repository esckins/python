import socket

PORT_NO = 50500

client_sock = None
try:
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    client_sock.connect(('185.86.86.10', PORT_NO))
    print('connected...')
    
    while True:
        s = input('Text:')
        buf = s.encode()
        client_sock.send(buf)
        if s == 'quit':
            break
    
    client_sock.shutdown(socket.SHUT_RDWR)
    
except OSError as oserr:
    print(oserr)
finally:
    if client_sock:
        client_sock.close()


####
