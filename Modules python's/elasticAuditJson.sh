user=esckin
pass=admin
login=$user:$pass
/usr/bin/curl -u $login -X GET "http://10.30.30.10:9200/security-auditlog-*/_search" -H 'Content-Type: application/json' -d'


{
  "aggs": {
    "2": {
      "terms": {
        "field": "audit_request_effective_user.keyword",
        "order": {
          "_count": "desc"
        },
        "size": 50
      },
      "aggs": {
        "3": {
          "terms": {
            "field": "audit_category.keyword",
            "order": {
              "_count": "desc"
            },
            "size": 50
          }
        }
      }
    }
  },
  "size": 0,
  "stored_fields": [
    "*"
  ],
  "script_fields": {},
  "docvalue_fields": [
    {
      "field": "@timestamp",
      "format": "date_time"
    }
  ],
  "_source": {
    "excludes": []
  },
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "match_all": {}
        },
        {
          "range": {
            "@timestamp": {
              "gte": "now-1d",
              "lte": "now",
              "format": "strict_date_optional_time"
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}
'
