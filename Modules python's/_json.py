"""
Created on Thu Oct 23 16:37:18 2021

@author: Seçkin
"""

import json

person_string = '{"name":"Ahmet", "languages":["python","Java"]}'
person_dict = {"name": "Ahmet","languages": ["Python","Java"] }

# JSON string to Dict
# result = json.loads(person_string)
# result = result["name"]
# result = result["languages"]

# with open("person.json") as f:
#     data = json.load(f)
#     print(data["name"])
#     print(data["languages"])


# Dict to JSON string
# result = json.dumps(person_dict)
# print(type(result))

# with open("person.json","w") as f:
#     json.dump(person_dict, f)

# person_dict = json.loads(person_string)

# result = json.dumps(person_dict, indent= 4, sort_keys= True)
# print(person_dict)
# print(result)



#write json doc to file

person_dict = {"name": "mehmet",
"languages": ["Island", "Soul"],
"married": False,
"age": 25
}

#person.txt
with open('person.json', 'w') as json_file:
  json.dump(person_dict, json_file)



##json formatter
import json

person_string = '{"name": "Deli", "languages": "France", "numbers": [1, 0.1, null]}'

# Getting dictionary
person_dict = json.loads(person_string)

# Pretty Printing JSON string back
print(json.dumps(person_dict, indent = 4, sort_keys=True))





