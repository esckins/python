import socket

PORT_NO = 50500

try:
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    server_sock.bind(('', PORT_NO))
    server_sock.listen(8)
    print('waiting for connection...')
    client_sock = server_sock.accept()
    
    # send and recv
    
    client_sock.shudown(socket.SHUT_RDWR)
    client_sock.close()
    server_sock.close()
    
except OSError as oserr:
    print(oserr)
