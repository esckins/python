##powered by Harun Ozgur Tiskaya

import requests
import base64
import json
import pandas as pd
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates
import matplotlib.pyplot as pp
import urllib3
import pandas as pd
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import json

# url ='https://dynatraceone.esckin.intra/e/111-c5a2-1abc-9468-2222b6b2eaa2/api/v2/metrics/query?metricSelector=builtin:service.keyRequest.response.time:filter(and(in("dt.entity.service_method",entitySelector("type(service_method),entityName(~"/api/v1.0/Card/GetDashboardCardInfo~")")))):splitBy():setUnit(MilliSecond):percentile(90):auto:sort(value(percentile(90),descending))&from=-8d&to=-1d&resolution=1d'

envProd = "https://dynatraceone.esckin.intra/e/111-c5a2-1abc-9468-2222b6b2eaa2"

headers = {
    'Authorization': 'Api-Token xyz123'
}

metricAPIURL = "/api/v2/metrics/query?metricSelector="
metrics = ['builtin:service.keyRequest.count.total', 'builtin:service.keyRequest.response.client']

splitURL = [':splitBy("dt.entity.service_method"):sum:auto:sort(value(sum,descending)),',
            ':splitBy("dt.entity.service_method"):avg:auto:sort(value(avg,descending)),',
            ':splitBy("dt.entity.service_method"):max:auto:sort(value(max,descending)),']

operationURL = ':filter(and(in("dt.entity.service_method",entitySelector("type(service_method),entityName.equals(~"#ENTITY#~")"))))'
# aggregation=['sum','avg']
# splitURL = ':splitBy("dt.entity.service_method"):#aggregation#:auto:sort(value(#aggregation#,descending))'
# splitURL = ':splitBy("dt.entity.service_method"):#AGGREGATION#:auto:sort(value(#AGGREGATION#,descending))'
timeFrameURL = '&from=now-1m&resolution=1m'
serviceNames = ["SendFast"]

# jsons = []

for metric in metrics:

    for serviceName in serviceNames:
        # print(serviceName)

        for i in range(len(splitURL)):
            url = envProd + metricAPIURL + str(metric) + operationURL.replace("#ENTITY#", serviceName) + splitURL[
                i] + timeFrameURL
            response = requests.get(url, headers=headers, verify=False)

            print(url)
            print('#####################')
            print(response.json())

            # jsons.append(response.json())
