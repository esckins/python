# -*- coding: utf-8 -*-
"""
Created on Sun Oct 31 00:44:53 2021

@author: Seçkin
"""

numbers = [1,2,3,4,5]

for a in numbers:
    print('Hello')


###
names = ['ahmet','mehmet','sena']

for name in names:
    print(f'my name is {names}')

name = 'xyz abc'

for n in name:
    print(n)

tuple = [(1,2),(1,3),(3,5),(5,7)]

for a,b in tuple:
    print(a,b)

d = {'k1':1, 'k2':2, 'k3':3}

for key,value in d.items():
    print(key, value)
