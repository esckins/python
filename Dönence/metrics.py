# Modülleri import ettim.
import cx_Oracle
import pandas as pd
import numpy as np
#from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import time


# DB tns bilgileri ile connection kurduğum kısım.
dns_tns = cx_Oracle.makedsn('db-scan.esckin.intra', '1954', service_name='test')
conn = cx_Oracle.connect(user='esckin', password=r'aswwQ1WS2s', dsn=dns_tns, encoding='UTF-8')
curr = conn.cursor()






l = [2481, 2482, 2507, 2508, 2509, 2510, 2483, 2484]

names = ['test - Connected count', 'test2 - Available count', 'test3 - Available count', 'test4 - Connected count', 'a - Connected count', 'a1 - Available count', a2 - Available count', 'a3 - Connected count']

figure, axis = plt.subplots(nrows=2, ncols=4)
plt.subplots_adjust(hspace=0.5)

for n, name in enumerate(names):
    
    print(n)
    
    ax = plt.subplot(2, 4, n + 1)
    
    # Select sorgusu, null olanları da direkts sql içerisinde dikkate almadım.
    sql_query = f'''select TARIH,MEASURE from test.db where METRICID= {l[n]} and tarih > sysdate - 7 '''
    
    # print(sql_query)
    
    
    ax.set_title(name.upper())
    # ax.get_legend().remove()
    # ax.set_xlabel("")
    
    df = pd.read_sql_query(sql_query, con=conn).sort_values(by=['TARIH'], ignore_index=True)
    
    df.plot(x='TARIH', y='MEASURE',ax=ax)
    plt.show()
    
    
    
    
    # dataset = df.to_numpy(dtype='float32')

#development by Harun Özgür Tiskaya
