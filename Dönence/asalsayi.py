# -*- coding: utf-8 -*-
"""
Created on Sun Oct 31 00:52:16 2021

@author: Seçkin
"""

sayi = int(input('sayi: '))
asal = True

if sayi == 1:
    asal = False

for i in range(2, sayi):
    if (sayi % i == 0):
        asal = False
        break

if asal:
    print('sayı asaldir.')
else:
    print('sayı asal degildir.')
