#!/bin/python

import math
import os
import random
import re
import sys

#
# Complete the 'fizzBuzz' function below.
#
# The function accepts INTEGER n as parameter.
#
def fizzBuzz(n=100):
    for i in range(1, n + 1):
        if i % 3==0 and i % 5==0:
            print('FizzBuzz')
        elif i % 3==0:
            print('Fizz')
        elif i % 5==0:
            print('Buzz')
        else:
            print(i)
if __name__ == '__main__':
    n = int(raw_input().strip())

    fizzBuzz(n)





############################################

1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
